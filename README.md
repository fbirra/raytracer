# README #

This is the skeleton code for a ray tracing engine. Minimal features implemented.

### What is this repository for? ###

* Learn the internals of a basic ray tracing engine
* Add new features: new primitives, hierarchies, area lights, antialiasing, textures, ...

### How do I get set up? ###

* Clone repository or download source
* create a C++ console project (eclipse, Xcode, Visual Studio, ...)
* Add all .cpp files to your project

### How does it work? ###

* Check main.cpp to see how to create a scene
* Run the program
* Open the output file (image.tga)