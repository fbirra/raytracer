#include <iostream>

#include "Scene.h"
#include "Sphere.h"
#include "Camera.h"
#include "SimpleSensor.h"

void oneSphereScene()
{
    Scene s;
    
    Material green(Color(0.0, 0.2, 0.0), Color(0.0, 0.8, 0.0), Color(1,1,1), Color(0.2,0.2,0.2), Color::black, 5, 1);
    
    TransformSeq ts1; ts1.addScale(1.2,1.2,0.7); ts1.addTranslation(0, 0, -1.2);
    
    s.addInstance(new Instance(new Sphere(), ts1, green));
    
    s.addLight(new PointLight(Vector3(0,10,10)));
    
    Camera cam(Vector3(0,0,10));
    ImageSensor *is = new SimpleSensor(600,400);
    
    cam.setSensor(is);
    cam.capture(s);
}

void threeSpheresScene()
{
    Scene s;
    
    Material red(Color(0.0, 0.0, 0.0), Color(0.4, 0.0, 0.0), Color(0.4,0.4,0.4), Color(0.7, 0.5, 0.5), Color::black, 50, 1);
    Material blue(Color(0.0, 0.0, 0.0), Color(0.0, 0.0, 0.0), Color(0.4, 0.4, 0.4), Color(0.5, 0.5, 0.7), Color::black, 50, 1);
    Material green(Color(0.0, 0.1, 0.0), Color(0.0, 0.6, 0.0));
    
    TransformSeq ts0; ts0.addScale(1.5, 1.5, 1.5); ts0.addTranslation(0, 0, 0);
    TransformSeq ts1; ts1.addScale(0.5, 1.0, 1.0); ts1.addTranslation(0, 0, 1);
    TransformSeq ts2; ts2.addScale(0.5, 0.5, 0.5); ts2.addTranslation(-1.0, 1.6, 1.0);
    TransformSeq ts3; ts3.addScale(0.5, 0.5, 0.5); ts3.addTranslation(1.0, 1.6, 1.0);
    
    s.addInstance(new Instance(new Sphere(), ts0, red));
    s.addInstance(new Instance(new Sphere(), ts1, green));
    s.addInstance(new Instance(new Sphere(), ts2, blue));
    s.addInstance(new Instance(new Sphere(), ts3, blue));
    
    s.addLight(new PointLight(Vector3(0,10,10)));
    s.addLight(new DirectionalLight(Vector3(0.0,-1.0, 0.0)));

    s.setBackground(Color(0.3, 0.7, 0.8));
    
    Camera cam(Vector3(0,0,10));
    ImageSensor *is = new SimpleSensor(600,400);
    
    cam.setSensor(is);
    cam.capture(s);
}

void tests()
{
    for(int i=0; i<10; i++) {
        Vector3 a = Vector3::randomDir();
        Vector3 n = Vector3::randomDir();
        
        Vector3 b = a.reflected(n);
        
        std::cout << a.length() << " " << a;
        std::cout << n.length() << " " << n;
        std::cout << b.length() << " " << b;
        std::cout << a.dot(n) << "==" << b.dot(n) << "?" << std::endl;
    }
}
int main(int argc, char *argv[])
{
    //oneSphereScene();
    threeSpheresScene();
    
    return 0;
}




