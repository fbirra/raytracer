//
//  Light.h
//  RayTracer
//
//  Created by Fernando Pedro Birra on 25/05/15.
//  Copyright (c) 2015 FCT/UNL. All rights reserved.
//

#ifndef __RayTracer__Light__
#define __RayTracer__Light__

#include <stdio.h>

#include "Color.h"
#include "Vector3.h"

class Light {
  
public:
    Light(): col(1.0, 1.0, 1.0) {};
    Light(const Color &col) : col(col) {};
    virtual ~Light() {};

    // Override this in subclasses
    // Returns a normalized vector pointing to the light source
    virtual Vector3 sampleLightDir(const Vector3 &from) const = 0; // Returns a direction pointing to the light
    
public:
    // Data members
    Color col;  // light's color

};

class PointLight : public Light {
public:
    PointLight() : Light(), pos() { };
    PointLight(const Color &col) : Light(col), pos() {};
    PointLight(const Vector3 &pos) : Light(), pos(pos) {};
    PointLight(const Vector3 &pos, const Color &col) : Light(col), pos(pos) {};
    virtual ~PointLight() {};
    
    virtual Vector3 sampleLightDir(const Vector3 &from) const { Vector3 l = pos-from; l.normalize(); return l; }
public:
    Vector3 pos;
};

class DirectionalLight : public Light {
public:
    DirectionalLight(): Light(), dir(0,-1,0) {};
    DirectionalLight(const Vector3 &dir) : Light(), dir(dir) { this->dir.normalize(); };
    DirectionalLight(const Color &col, const Vector3 &dir): Light(col), dir(dir) { this->dir.normalize(); }
    virtual ~DirectionalLight() {};
    
    virtual Vector3 sampleLightDir(const Vector3 &from) const { return - dir; }
public:
    Vector3 dir;
};
#endif /* defined(__RayTracer__Light__) */
