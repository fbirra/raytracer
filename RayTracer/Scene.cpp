//
//  Scene.cpp
//  RayTracer
//
//  Created by Fernando Birra on 08/11/13.
//  Copyright (c) 2013 FCT/UNL. All rights reserved.
//
#include <math.h>

#include "Scene.h"

void Scene::setCameraPos(const Vector3 &eye)
{
    this->eye = eye;
}

void Scene::setBackground(const Color &background)
{
    this->background = background;
}

void Scene::addInstance(Instance *obj)
{
    instances.push_back(obj);
}

void Scene::addLight(Light *light)
{
    lights.push_back(light);
}

Color Scene::traceRay(const Ray &r, int depth)
{
    double tmin = INFINITE;
    Instance *obj=NULL;
    Hit h;
 
    if(depth == maxDepth) return Color::black;
    
    // process each primitive instace
    for(std::vector<Instance *>::iterator it=instances.begin(); it!=instances.end(); it++)
    {
        Hit hi;
        real t=INFINITE; Instance *ins = *it;
        if(ins->intersects(r, t, hi)) {
            if(t < tmin) {
                // This intersection is closest to the ray origin
                tmin = t;
                obj = ins;
                h = hi;
            }
        }
    }
    
    if(tmin!=INFINITE) // We have a hit
    {
        Color diffuse = Color::black;
        Color specular = Color::black;
        Color reflected = Color::black;
        Color transmitted = Color::black;
        
        Vector3 P = h.p;
        Vector3 N = h.n;
        
        for(std::vector<Light *>::iterator il=lights.begin(); il!=lights.end(); il++) {
            
            // Get light direction
            Vector3 L = (*il)->sampleLightDir(P);
            
            // Create a light ray
            Ray rl(P, L);
            
            // Test for intersection
            bool lightIsVisible = true;
            for(std::vector<Instance *>::iterator it=instances.begin(); it!=instances.end(); it++)
            {
                if((*it)->intersects(rl)) {
                    lightIsVisible = false;
                    break;
                }
            }
            
            // Perform illumination model computations
            real ndotl, rdotv;
            
            if(lightIsVisible) {
                // Compute direction of reflected light
                Vector3 R = L.reflected(N);
                // Compute direction to viewer
                Vector3 V = this->eye - P;
                V.normalize();
                ndotl = MAX(0, dot(N, L));
                rdotv = MAX(0, dot(R, V));
            }
            else {
                ndotl = 0;
                rdotv = 0;
            }
            
            diffuse += obj->m.kd * ndotl;
            specular += obj->m.ks * ::pow(rdotv, obj->m.ns);
            
        }
        
        if( obj->m.isReflective ) {
            Ray rr = r.reflected(P, N); rr.d *= -1;
            reflected = this->traceRay(rr, depth + 1) * obj->m.kr;
        }   
        
        if( obj->m.isTransmissive) {
            // TO DO
            transmitted = Color::black;
        }
        return obj->m.ka + diffuse + specular + reflected;
    }
    return background;
}