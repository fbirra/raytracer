//
//  Scene.h
//  RayTracer
//
//  Created by Fernando Birra on 08/11/13.
//  Copyright (c) 2013 FCT/UNL. All rights reserved.
//

#ifndef __RayTracer__Scene__
#define __RayTracer__Scene__

#include <iostream>
#include <vector>

#include "Instance.h"
#include "Light.h"

#define SCENE_DEFAULT_MAX_DEPTH 5

class Scene {
    
public:
    Scene() : maxDepth(SCENE_DEFAULT_MAX_DEPTH), background(0,0,0) {};
    virtual ~Scene()  {};
    
    virtual void addInstance(Instance *obj);
    virtual void addLight(Light *light);
    
    virtual void setCameraPos(const Vector3 &eye);
    
    virtual void setBackground(const Color &background);
    virtual Color traceRay(const Ray &r, int depth=0);
    
    // data members
    int maxDepth;
    Color background;
    Vector3 eye;
    std::vector<Instance *> instances;
    std::vector<Light *> lights;
};

#endif /* defined(__RayTracer__Scene__) */
